package com.crypto.cryptotrader.viewhelpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.models.ExchangesEnum;

/**
 * Created by aditya on 1/3/18.
 *
 */

public class ExchangeAdapter extends BaseAdapter {
    private ExchangesEnum[] exchangeValues = ExchangesEnum.values();
    private static class ExchangeHolder {
        private TextView label;
        private ImageView logo;

        ExchangeHolder(View view) {
            label = view.findViewById(R.id.exchange_name);
            logo = view.findViewById(R.id.exchange_image);
        }

        private TextView getLabel() {
            return label;
        }

        private ImageView getLogo() {
            return logo;
        }
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View gridItem;
        LayoutInflater inflater = (LayoutInflater) parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ExchangeHolder exchangeHolder;
        if (convertView == null && inflater != null) {
            gridItem = inflater.inflate(R.layout.exchange_single, null);
            exchangeHolder = new ExchangeHolder(gridItem);
        } else {
            gridItem = convertView;
            //noinspection ConstantConditions
            exchangeHolder = (ExchangeHolder)gridItem.getTag(R.id.HOLDER_TAG);
        }

        exchangeHolder.getLabel().setText(exchangeValues[position].getName());
        exchangeHolder.getLogo().setImageResource(exchangeValues[position].getImageResource());
        gridItem.setTag(R.id.HOLDER_TAG, exchangeHolder);
        gridItem.setTag(R.id.EXCHANGE_TAG, exchangeValues[position]);
        return gridItem;
    }

    @Override
    public Object getItem(int position) {
        return exchangeValues[position]; //returns list item at the specified position
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return exchangeValues.length; //returns total of items in the list
    }
}
