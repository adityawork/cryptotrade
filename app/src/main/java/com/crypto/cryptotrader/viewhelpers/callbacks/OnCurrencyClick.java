package com.crypto.cryptotrader.viewhelpers.callbacks;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.apis.Binance.BinanceApiClient;
import com.crypto.cryptotrader.apis.Binance.BinanceApiService;
import com.crypto.cryptotrader.apis.Bittrex.BittrexApiClient;
import com.crypto.cryptotrader.apis.Bittrex.BittrexApiService;
import com.crypto.cryptotrader.utils.BackgroundThreadHelper;
import com.crypto.cryptotrader.utils.BookmarkPreferenceHelper;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;
import com.crypto.cryptotrader.models.ExchangesEnum;
import com.crypto.cryptotrader.models.binance.BinanceCurrencyData;
import com.crypto.cryptotrader.models.bittrex.BittrexCurrencyLast;
import com.crypto.cryptotrader.models.bittrex.BittrexCurrencyLastResult;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aditya on 16/3/18.
 *
 */

public class OnCurrencyClick implements AdapterView.OnItemClickListener {
    private List<CommonCurrencyData> currenciesMetaData = new ArrayList<>();

    public void onItemClick(AdapterView<?> adapterView, final View view, int i, long l) {
        ImageView selectedStatus = view.findViewById(R.id.currency_selected);
        if(currenciesMetaData.size() < 1) {
            currenciesMetaData = BookmarkPreferenceHelper.getMarketCurrencies(getCurrency(view.getTag(R.id.LABEL_TAG).toString()));
        }
        if (selectedStatus.getVisibility() == View.VISIBLE) {
            selectedStatus.setVisibility(View.INVISIBLE);
            view.setSelected(false);
            BackgroundThreadHelper.getBackgroundHandler().post(new Runnable() {
                @Override
                public void run() {
                    removeCurrency(view.getTag(R.id.LABEL_TAG).toString());
                }
            });
        } else {
            selectedStatus.setVisibility(View.VISIBLE);
            view.setSelected(true);
            BackgroundThreadHelper.getBackgroundHandler().post(new Runnable() {
                @Override
                public void run() {
                    addCurrency(view.getTag(R.id.LABEL_TAG).toString());
                }
            });
        }
    }

    /**
     * Removes the already added currency
     * @param tag Custom tag of a currency
     */
    private void removeCurrency(String tag) {

        CommonCurrencyData selectedCurrency = null;
        for (CommonCurrencyData currency : currenciesMetaData) {
            if (currency.getTag().equalsIgnoreCase(tag)) {
                selectedCurrency = currency;
            }
        }

        if(selectedCurrency != null) {
            currenciesMetaData.remove(selectedCurrency);
            BookmarkPreferenceHelper.setMarketCurrency(selectedCurrency, getCurrency(tag), BookmarkPreferenceHelper.MODE_REMOVE);
            BookmarkPreferenceHelper.setBookmarkPosition(tag, BookmarkPreferenceHelper.MODE_REMOVE);
        }
    }

    /**
     * Adds the selected currency
     * Fetches the updated values
     * @param tag Custom tag of a currency
     */
    private void addCurrency(String tag) {

        boolean isSaved = BookmarkPreferenceHelper.isSaved(tag);
        if(isSaved) {
            return;
        }
        BookmarkPreferenceHelper.setBookmarkPosition(tag, BookmarkPreferenceHelper.MODE_ADD);
        String tagArray[] = tag.split("-");
        if(tagArray.length != 3) {
            Log.d("TAG", "Illegal tags");
        }
        String baseCurrency = tagArray[1];
        String currency = tagArray[2];
        ExchangesEnum exchange = getCurrency(tag);
        if (exchange != null) {
            switch(exchange) {
                case BINANCE:
                    BinanceApiService binanceApiService = BinanceApiClient.getClient().create(BinanceApiService.class);
                    final Call<BinanceCurrencyData> binanceData = binanceApiService.getCurrencyData(currency+baseCurrency);
                    binanceData.enqueue(new Callback<BinanceCurrencyData>() {
                        @Override
                        public void onResponse(@Nullable Call<BinanceCurrencyData> call, @Nullable Response<BinanceCurrencyData> response) {
                            BinanceCurrencyData binanceCurrenciesData = null;
                            if (response != null) {
                                binanceCurrenciesData = response.body();
                            }
                            if (binanceCurrenciesData != null) {
                                CommonCurrencyData binanceFormatedData = binanceCurrenciesData.formatToSave();
                                BookmarkPreferenceHelper.setMarketCurrency(binanceFormatedData, ExchangesEnum.BINANCE, BookmarkPreferenceHelper.MODE_ADD);
                            }
                        }

                        @Override
                        public void onFailure(@Nullable Call<BinanceCurrencyData> call, @Nullable Throwable t) {
                            Log.e(ExchangesEnum.BINANCE.getName(), t != null ? t.toString() : "Request failed");
                        }
                    });
                    break;

                case BITTREX:
                    BittrexApiService bittrexApiService = BittrexApiClient.getClient().create(BittrexApiService.class);
                    final Call<BittrexCurrencyLast> bittrexData = bittrexApiService.getCurrencyData(baseCurrency+"-"+currency);
                    bittrexData.enqueue(new Callback<BittrexCurrencyLast>() {
                        @Override
                        public void onResponse(@Nullable Call<BittrexCurrencyLast> call, @Nullable Response<BittrexCurrencyLast> response) {
                            BittrexCurrencyLast bittrexCurrencyLast = null;
                            if (response != null) {
                                bittrexCurrencyLast = response.body();
                            }
                            if (bittrexCurrencyLast != null) {
                                BittrexCurrencyLastResult[] bittrexCurrencyLastResult = bittrexCurrencyLast.getResult();
                                if(bittrexCurrencyLastResult != null) {
                                    CommonCurrencyData commonCurrencyData = bittrexCurrencyLastResult[0].formatToSave();
                                    BookmarkPreferenceHelper.setMarketCurrency(commonCurrencyData, ExchangesEnum.BITTREX, BookmarkPreferenceHelper.MODE_ADD);
                                }
                            }
                        }

                        @Override
                        public void onFailure(@Nullable Call<BittrexCurrencyLast> call, @Nullable Throwable t) {
                            Log.e(ExchangesEnum.BINANCE.getName(), t != null ? t.toString() : "Request failed");
                        }
                    });
                    break;

                default:
                    Log.d("Currency", "No currency selected");
                    break;
            }
        }
    }

    /**
     * Gets the exchange from the custom currency tag
     * @param tag Custom currency tag
     * @return exchange
     */
    private ExchangesEnum getCurrency(String tag) {
        String exchangeTag = tag.split("-")[0];
        for(ExchangesEnum exchange : ExchangesEnum.values()) {
            if(exchange.getTagName().equalsIgnoreCase(exchangeTag)) {
                return exchange;
            }
        }
        return null;
    }
}
