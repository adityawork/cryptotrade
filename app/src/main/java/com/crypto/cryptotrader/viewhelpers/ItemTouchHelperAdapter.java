package com.crypto.cryptotrader.viewhelpers;

/**
 * Created by aditya on 23/3/18.
 *
 */

public interface ItemTouchHelperAdapter {
    @SuppressWarnings("UnusedReturnValue")
    boolean onItemMove(int fromPosition, int toPosition);
    void onItemDismiss(int position);
}
