package com.crypto.cryptotrader.viewhelpers.callbacks;

import android.support.v7.widget.RecyclerView;

/**
 * Created by aditya on 23/3/18.
 *
 */

public interface OnStartDragListener {
    void onStartDrag(RecyclerView.ViewHolder viewHolder);
}
