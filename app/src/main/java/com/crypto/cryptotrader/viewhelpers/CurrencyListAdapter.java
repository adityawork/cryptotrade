package com.crypto.cryptotrader.viewhelpers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.utils.BookmarkPreferenceHelper;
import com.crypto.cryptotrader.models.ExchangesEnum;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;

import java.util.List;

/**
 * Created by qoini on 04/04/18.
 *
 */

public class CurrencyListAdapter extends BaseAdapter {
    private List<CommonCurrencyData> commonCurrencyData;

    static class CurrencyViewHolder {
        private ImageView selected;
        private ImageView icon;
        private TextView market;
        private TextView value;
        private TextView currency;

        private CurrencyViewHolder(View item) {
            selected = item.findViewById(R.id.currency_selected);
            icon = item.findViewById(R.id.currency_icon);
            currency = item.findViewById(R.id.single_text);
            value = item.findViewById(R.id.currency_value);
            market = item.findViewById(R.id.market);
        }

        ImageView getSelected() {
            return selected;
        }

        public ImageView getIcon() {
            return icon;
        }

        TextView getMarket() {
            return market;
        }

        TextView getValue() {
            return value;
        }

        TextView getCurrency() {
            return currency;
        }
    }

    public CurrencyListAdapter(List<CommonCurrencyData> commonCurrencyData) {
        this.commonCurrencyData = commonCurrencyData;
    }

    @Override
    public int getCount() {
        return commonCurrencyData.size();
    }

    @Override
    public CommonCurrencyData getItem(int position) {
        return commonCurrencyData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CommonCurrencyData itemData = getItem(position);
        ExchangesEnum exchangeData = ExchangesEnum.valueOf(itemData.getExchange());

        View gridItem;
        CurrencyViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //noinspection ConstantConditions
            gridItem = inflater.inflate(R.layout.currency_single, null, false);
            holder = new CurrencyViewHolder(gridItem);
        } else {
            gridItem = convertView;
            holder = (CurrencyViewHolder) convertView.getTag(R.id.HOLDER_TAG);
        }
        if(BookmarkPreferenceHelper.isSaved(itemData.getTag())) {
            holder.getSelected().setVisibility(View.VISIBLE);
        } else {
            holder.getSelected().setVisibility(View.INVISIBLE);
        }

        holder.getIcon().setImageResource(exchangeData.getImageResource());
        holder.getCurrency().setText(getItem(position).getDisplay());
        holder.getValue().setText(itemData.getCurrencyValue());
        holder.getMarket().setText(itemData.getExchange());
        gridItem.setTag(R.id.LABEL_TAG, itemData.getTag());
        gridItem.setTag(R.id.HOLDER_TAG, holder);

        return gridItem;
    }
}
