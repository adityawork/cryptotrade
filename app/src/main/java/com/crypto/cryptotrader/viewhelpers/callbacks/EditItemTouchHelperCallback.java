package com.crypto.cryptotrader.viewhelpers.callbacks;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;

import com.crypto.cryptotrader.utils.BookmarkPreferenceHelper;
import com.crypto.cryptotrader.viewhelpers.BookmarkAdapter;

import java.util.ArrayList;

/**
 * Created by aditya on 23/3/18.
 *
 */

public class EditItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final BookmarkAdapter mAdapter;

    public EditItemTouchHelperCallback(BookmarkAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN |  ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        ArrayList<String> bookmarksReorder = new ArrayList<>();
        BookmarkAdapter.BookmarkHolder holder;
        for (int childCount = recyclerView.getChildCount(), i = 0; i < childCount; ++i) {
            holder = (BookmarkAdapter.BookmarkHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(i));
            bookmarksReorder.add(String.valueOf(holder.itemView.getTag()));
        }
        BookmarkPreferenceHelper.updateBookmarkPosition(bookmarksReorder);
    }
}