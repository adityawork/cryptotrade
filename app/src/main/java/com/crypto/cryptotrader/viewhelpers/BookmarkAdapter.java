package com.crypto.cryptotrader.viewhelpers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.models.ExchangesEnum;
import com.crypto.cryptotrader.viewhelpers.callbacks.OnStartDragListener;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;
import java.util.Collections;
import java.util.List;

/**
 * Created by aditya on 20/3/18.
 *
 */

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarkHolder> implements ItemTouchHelperAdapter {

    private List<String> saved_bookmarks;
    private final OnStartDragListener mDragStartListener;
    private OnItemClickListener mItemClickListener;
    private List<CommonCurrencyData> commonCurrencyData;
    private int positiveColor;
    private int negativeColor;

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    @SuppressWarnings("unused")
    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    public class BookmarkHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView bookExchange;
        private ImageView bookIcon;
        private TextView bookCurrency;
        private TextView bookCurrValue;
        private TextView bookCurrRelVal;
        private RelativeLayout bookCurrGrid;

        private BookmarkHolder(RelativeLayout v) {
            super(v);
            bookIcon       = v.findViewById(R.id.bookmark_icon);
            bookExchange   = v.findViewById(R.id.bookmark_exchange);
            bookCurrency   = v.findViewById(R.id.bookmark_currency);
            bookCurrValue  = v.findViewById(R.id.bookmark_currency_value);
            bookCurrRelVal = v.findViewById(R.id.bookmark_currency_rel_per);
            bookCurrGrid   = v.findViewById(R.id.bookmark_layout);
            itemView.setOnClickListener(this);
        }

        private void bind(int position) {
            String notAvailable = itemView.getContext().getString(R.string.not_available);
            String itemTag = saved_bookmarks.get(position);
            String exchange = notAvailable;
            String value = notAvailable;
            String display = notAvailable;
            String change = notAvailable;
            Boolean rise = false;
            for(CommonCurrencyData  currencyData : commonCurrencyData){
                if(currencyData.getTag().equalsIgnoreCase(itemTag)) {
                    exchange = currencyData.getExchange();
                    display  = currencyData.getDisplay();
                    value    = currencyData.getCurrencyValue();
                    change   = currencyData.getDisplayPriceChange();
                    rise     = currencyData.isRise();
                }
            }
            bookCurrency.setText(display);
            bookExchange.setText(exchange);
            bookCurrValue.setText(value);
            bookCurrRelVal.setText(change);
            itemView.setTag(itemTag);
            if(exchange.equalsIgnoreCase(ExchangesEnum.BITTREX.getName())) {
                bookIcon.setImageResource(ExchangesEnum.BITTREX.getImageResource());
            } else {
                bookIcon.setImageResource(ExchangesEnum.BINANCE.getImageResource());
            }

            if (rise) {
                bookCurrRelVal.setTextColor(positiveColor);
            } else {
                bookCurrRelVal.setTextColor(negativeColor);
            }
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                //noinspection deprecation
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }

    public BookmarkAdapter(
            List<String> bookmarks,
            List<CommonCurrencyData> commonCurrenciesData,
            Context context,
            OnStartDragListener dragListner) {
        saved_bookmarks = bookmarks;
        mDragStartListener = dragListner;
        commonCurrencyData = commonCurrenciesData;
        positiveColor = context.getResources().getColor(R.color.positive);
        negativeColor = context.getResources().getColor(R.color.negative);
    }

    @Override
    public int getItemCount() {
        return saved_bookmarks.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public BookmarkAdapter.BookmarkHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bookmark_single, parent, false);
        return new BookmarkHolder(v);
    }

    @Override
    public void onBindViewHolder(BookmarkHolder holder, int position) {
        final BookmarkHolder viewHolder = holder;
        holder.bind(position);
        holder.bookCurrGrid.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mDragStartListener.onStartDrag(viewHolder);
                return false;
            }
        });
    }

    @Override
    public void onItemDismiss(int position) {
        saved_bookmarks.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < saved_bookmarks.size() && toPosition < saved_bookmarks.size()) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(saved_bookmarks, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(saved_bookmarks, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
        }
        return true;
    }

}