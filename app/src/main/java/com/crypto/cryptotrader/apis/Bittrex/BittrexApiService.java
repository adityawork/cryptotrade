package com.crypto.cryptotrader.apis.Bittrex;

import com.crypto.cryptotrader.models.bittrex.BittrexCurrencyLast;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by aditya on 7/3/18.
 *
 */

public interface BittrexApiService {
    @GET("api/v1.1/public/getmarketsummaries")
    Call<BittrexCurrencyLast> getCurrencies();

    @GET("api/v1.1/public/getmarketsummary")
    Call<BittrexCurrencyLast> getCurrencyData(@Query("market") String market);
}
