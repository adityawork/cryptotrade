package com.crypto.cryptotrader.apis.Bittrex;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by aditya on 7/3/18.
 *
 */

public class BittrexApiClient {
    private static final String BASE_URL = "https://bittrex.com/";
    private static Retrofit retrofit = null;

    /**
     * Returns the retrofit api client
     * @return retrofit client
     */
    public static Retrofit getClient() {
        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
