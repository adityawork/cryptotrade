package com.crypto.cryptotrader.apis.Binance;

import com.crypto.cryptotrader.models.binance.BinanceCurrencyData;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by aditya on 7/3/18.
 *
 */

public interface BinanceApiService {
    @GET("api/v1/ticker/24hr")
    Call<List<BinanceCurrencyData>> getCurrenciesData();

    @GET("api/v1/ticker/24hr")
    Call<BinanceCurrencyData> getCurrencyData(@Query("symbol") String symbol);
}
