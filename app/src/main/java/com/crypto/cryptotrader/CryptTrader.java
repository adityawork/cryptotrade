package com.crypto.cryptotrader;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;

/**
 * Created by qoini on 06/04/18.
 *
 */

public class CryptTrader extends Application {
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
    }

    public static Context getContext() {
        return context;
    }
}
