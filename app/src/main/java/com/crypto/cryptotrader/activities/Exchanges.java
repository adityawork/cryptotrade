package com.crypto.cryptotrader.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.fragments.ExchangeFragment;
import com.crypto.cryptotrader.fragments.ListCurrencyFragment;

/**
 * Created by aditya on 7/3/18.
 * It starts a fragment to display the available exchanges
 */

public class Exchanges extends AppCompatActivity implements ExchangeFragment.onItemClickCallBack {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exchange_frame);
        if(savedInstanceState == null) {
            ExchangeFragment exchangeFragment = new ExchangeFragment();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.exchange_frame, exchangeFragment);
            fragmentTransaction.addToBackStack("Exchange List").commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch(item.getItemId()) {
            case R.id.main :
                Intent backIntent = new Intent(this, MainActivity.class);
                startActivity(backIntent);
                return true;
            default :
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * A callback to display the list of currencies for the selected exchange
     * @param tag Custom tag of a currency
     */
    public void onItemClick(String tag) {

        Bundle bundle = new Bundle();
        bundle.putString("exchange", tag);
        ListCurrencyFragment listCurrencyFragment = new ListCurrencyFragment();
        listCurrencyFragment.setArguments(bundle);

        FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.exchange_frame, listCurrencyFragment);
        fragmentTransaction.addToBackStack("currencyList").commit();
    }
}
