package com.crypto.cryptotrader.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.services.PodcastPlayBackService;

/**
 * Created by aditya on 7/3/18.
 * This activity displays the bookmark fragment
 * Initiates a playback service and starts it on a callback
 */

public class MainActivity extends AppCompatActivity{
    PodcastPlayBackService mService;
    boolean mBound = false;
    Intent serviceIntent;

    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            PodcastPlayBackService.PodcastPlayBinder binder = (PodcastPlayBackService.PodcastPlayBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            mBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        serviceIntent = new Intent(this, PodcastPlayBackService.class);
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.add :
                Intent exchangeIntent = new Intent(this, Exchanges.class);
                startActivity(exchangeIntent);
                return true;
            case R.id.play :
                mService.setAction(PodcastPlayBackService.PLAY_MODE);
                startService(serviceIntent);
                invalidateOptionsMenu();
                return true;
            case R.id.pause :
                mService.setAction(PodcastPlayBackService.PAUSE_MODE);
                startService(serviceIntent);
                invalidateOptionsMenu();
                return true;
            default :
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem playItem = menu.findItem(R.id.play);
        MenuItem pauseItem = menu.findItem(R.id.pause);
        if(mService != null && mService.isPlaying()) {
            playItem.setVisible(false);
            pauseItem.setVisible(true);
        } else {
            playItem.setVisible(true);
            pauseItem.setVisible(false);
        }
        return true;
    }

    @Override
    protected void onResume() {
        invalidateOptionsMenu();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if(mBound) {
            unbindService(mConnection);
        }
        super.onDestroy();
    }
}
