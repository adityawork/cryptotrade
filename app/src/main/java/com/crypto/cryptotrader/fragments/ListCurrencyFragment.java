package com.crypto.cryptotrader.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.utils.BackgroundThreadHelper;
import com.crypto.cryptotrader.apis.Binance.BinanceApiClient;
import com.crypto.cryptotrader.apis.Binance.BinanceApiService;
import com.crypto.cryptotrader.apis.Bittrex.BittrexApiClient;
import com.crypto.cryptotrader.apis.Bittrex.BittrexApiService;
import com.crypto.cryptotrader.models.ExchangesEnum;
import com.crypto.cryptotrader.viewhelpers.callbacks.OnCurrencyClick;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;
import com.crypto.cryptotrader.models.binance.BinanceCurrencyData;
import com.crypto.cryptotrader.models.bittrex.BittrexCurrencyLast;
import com.crypto.cryptotrader.models.bittrex.BittrexCurrencyLastResult;
import com.crypto.cryptotrader.viewhelpers.CurrencyListAdapter;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by qoini on 04/04/18.
 * Displays the list of the currencies
 * With its values from the selected exchange
 */

public class ListCurrencyFragment extends Fragment {
    private List<CommonCurrencyData> commonCurrencyData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_currency_list, container, false);
    }

    /**
     * Gets the latest currencies with their values from the selected exchange
     * Populates the UI with the fresh values
     * @param view the view selected from onCreateView
     * @param savedInstanceState saved state of the previous instance
     */
    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView currencyList = (view).findViewById(R.id.currency_list);
        currencyList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        currencyList.setItemsCanFocus(false);
        String exchange = null;
        if (getArguments() != null) {
            exchange = getArguments().getString("exchange");
        }
        ExchangesEnum selectedExchange = ExchangesEnum.valueOf(exchange);
        switch(selectedExchange) {
            case BINANCE :
                BinanceApiService binanceApiService = BinanceApiClient.getClient().create(BinanceApiService.class);
                Call<List<BinanceCurrencyData>> call = binanceApiService.getCurrenciesData();
                call.enqueue(new Callback<List<BinanceCurrencyData>>() {
                    @Override
                    public void onResponse(@Nullable Call<List<BinanceCurrencyData>>call, @Nullable Response<List<BinanceCurrencyData>> response) {
                        List<BinanceCurrencyData> binanceData = response != null ? response.body() : null;
                        CommonCurrencyData binanceFormattedData;
                        if (binanceData != null && binanceData.size() > 0) {
                            for(BinanceCurrencyData binanceCurrency : binanceData) {
                                binanceFormattedData = binanceCurrency.formatToSave();
                                commonCurrencyData.add(binanceFormattedData);
                            }
                        }
                        updateCurrencyList(view);
                    }
                    @Override
                    public void onFailure(@Nullable Call<List<BinanceCurrencyData>>call, @Nullable Throwable t) {
                        Log.e(ExchangesEnum.BINANCE.getName(), t != null ? t.toString() : null);
                    }
                });
                break;

            case BITTREX :
                BittrexApiService bittrexApiService = BittrexApiClient.getClient().create(BittrexApiService.class);
                Call<BittrexCurrencyLast> bittrexApiData = bittrexApiService.getCurrencies();
                bittrexApiData.enqueue(new Callback<BittrexCurrencyLast>() {
                    BittrexCurrencyLast bittrexCurrenciesData;

                    @Override
                    public void onResponse(@Nullable Call<BittrexCurrencyLast> call, @Nullable Response<BittrexCurrencyLast> response) {
                        if (response != null) {
                            bittrexCurrenciesData = response.body();
                        }
                        BittrexCurrencyLastResult[] bittrexFormatedData = new BittrexCurrencyLastResult[0];
                        if (bittrexCurrenciesData != null) {
                            bittrexFormatedData = bittrexCurrenciesData.getResult();
                        }
                        if (bittrexFormatedData != null) {
                                for (BittrexCurrencyLastResult bittrexCurrency : bittrexFormatedData) {
                                    CommonCurrencyData binanceFormatedData = bittrexCurrency.formatToSave();
                                    commonCurrencyData.add(binanceFormatedData);
                                }
                            }
                            updateCurrencyList(view);
                    }

                    @Override
                    public void onFailure(@Nullable Call<BittrexCurrencyLast> call, @Nullable Throwable t) {
                        Log.e(ExchangesEnum.BITTREX.getName(), t != null ? t.toString() : "Request failed");
                    }
                });

                break;
        }
        updateCurrencyList(view);
    }

    /**
     * Generates the list view for the selected currency
     * @param view The generated layout for the fragment
     */
    public void updateCurrencyList(View view) {
        ListView currencyList = (view).findViewById(R.id.currency_list);
        TextView loadingView = (view).findViewById(R.id.list_loading);
        if(commonCurrencyData.size() > 1) {
            currencyList.setVisibility(View.VISIBLE);
            loadingView.setVisibility(View.GONE);
            currencyList.setAdapter(new CurrencyListAdapter(commonCurrencyData));
            currencyList.setOnItemClickListener(new OnCurrencyClick());
            currencyList.invalidate();
        } else {
            currencyList.setVisibility(View.GONE);
            loadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        BackgroundThreadHelper.closeBackgroundHandler();
        super.onDestroy();
    }
}
