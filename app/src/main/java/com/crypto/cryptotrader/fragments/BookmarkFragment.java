package com.crypto.cryptotrader.fragments;

import android.graphics.Rect;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.crypto.cryptotrader.apis.Binance.BinanceApiClient;
import com.crypto.cryptotrader.apis.Binance.BinanceApiService;
import com.crypto.cryptotrader.apis.Bittrex.BittrexApiClient;
import com.crypto.cryptotrader.apis.Bittrex.BittrexApiService;
import com.crypto.cryptotrader.utils.BookmarkPreferenceHelper;
import com.crypto.cryptotrader.viewhelpers.callbacks.OnStartDragListener;
import com.crypto.cryptotrader.viewhelpers.callbacks.EditItemTouchHelperCallback;
import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.viewhelpers.BookmarkAdapter;
import com.crypto.cryptotrader.models.ExchangesEnum;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;
import com.crypto.cryptotrader.models.binance.BinanceCurrencyData;
import com.crypto.cryptotrader.models.bittrex.BittrexCurrencyLast;
import com.crypto.cryptotrader.models.bittrex.BittrexCurrencyLastResult;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by aditya on 19/3/18.
 * Displays a grid of bookmarked currencies with latest values
 */

public class BookmarkFragment extends Fragment implements OnStartDragListener {
    private static final int SPACING = 10;
    private ItemTouchHelper mItemTouchHelper;
    private List<String> bookmarksPosition;
    private List<CommonCurrencyData> commonCurrenciesData = new ArrayList<>();
    private BookmarkAdapter bookmarkAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bookmarksPosition = BookmarkPreferenceHelper.getBookmarkPosition();
        commonCurrenciesData = BookmarkPreferenceHelper.getAllCurrencies();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bookmark_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView bookmarkGrid = view.findViewById(R.id.bookmarks);
        TextView noBookmark = view.findViewById(R.id.noBookmark);
        if(bookmarksPosition != null && bookmarksPosition.size() > 0) {
            bookmarkAdapter = new BookmarkAdapter(bookmarksPosition, commonCurrenciesData, getContext(), this);
            ItemTouchHelper.Callback callback =
                    new EditItemTouchHelperCallback(bookmarkAdapter);
            mItemTouchHelper = new ItemTouchHelper(callback);
            mItemTouchHelper.attachToRecyclerView(bookmarkGrid);
            bookmarkGrid.setAdapter(bookmarkAdapter);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
            bookmarkGrid.setLayoutManager(gridLayoutManager);
            bookmarkGrid.setPadding(SPACING, SPACING, SPACING, SPACING);
            bookmarkGrid.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    int position = parent.getChildViewHolder(view).getAdapterPosition();
                    if(position%2 != 0) {
                        outRect.left = SPACING;
                    }
                    outRect.bottom = SPACING;
                }
            });
            noBookmark.setVisibility(View.GONE);
            bookmarkGrid.setVisibility(View.VISIBLE);
        } else {
            noBookmark.setVisibility(View.VISIBLE);
            bookmarkGrid.setVisibility(View.GONE);
        }
    }

    /**
     * Fetches the new values of the currencies and updates the bookmark grid
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(bookmarksPosition == null) {
            return;
        }

        // BINANCE API FETCH
        BinanceApiService binanceApiService = BinanceApiClient.getClient().create(BinanceApiService.class);
        Call<List<BinanceCurrencyData>> binanceData = binanceApiService.getCurrenciesData();
        binanceData.enqueue(new Callback<List<BinanceCurrencyData>>() {

            @Override
            public void onResponse(@Nullable Call<List<BinanceCurrencyData>> call, @Nullable Response<List<BinanceCurrencyData>> response) {
                List<BinanceCurrencyData> binanceCurrenciesData;
                if (response == null) {
                    return;
                } else {
                    binanceCurrenciesData = response.body();
                }
                List<CommonCurrencyData> currenciesFormatedData = new ArrayList<>();
                if (binanceCurrenciesData != null) {
                    for (BinanceCurrencyData binanceCurrency : binanceCurrenciesData) {
                        CommonCurrencyData binanceFormatedData = binanceCurrency.formatToSave();
                        if(bookmarksPosition.contains(binanceFormatedData.getTag())) {
                            currenciesFormatedData.add(binanceFormatedData);
                        }
                    }
                }
                BookmarkPreferenceHelper.setMarketCurrencies(currenciesFormatedData, ExchangesEnum.BINANCE);
                bookmarksPosition = BookmarkPreferenceHelper.getBookmarkPosition();
                commonCurrenciesData = BookmarkPreferenceHelper.getAllCurrencies();
                if(bookmarkAdapter != null) {
                    bookmarkAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@Nullable Call<List<BinanceCurrencyData>> call, @Nullable Throwable t) {
                Log.e(ExchangesEnum.BINANCE.getName(), t != null ? t.toString() : "Request failed");
            }
        });

        // BITTREX API FETCH
        BittrexApiService bittrexApiService = BittrexApiClient.getClient().create(BittrexApiService.class);
        Call<BittrexCurrencyLast> bittrexApiData = bittrexApiService.getCurrencies();
        bittrexApiData.enqueue(new Callback<BittrexCurrencyLast>() {

            @Override
            public void onResponse(@Nullable Call<BittrexCurrencyLast> call, @Nullable Response<BittrexCurrencyLast> response) {
                BittrexCurrencyLast bittrexCurrenciesData;
                if (response == null) {
                    return;
                }
                bittrexCurrenciesData = response.body();
                List<CommonCurrencyData> currenciesFormatedData = new ArrayList<>();
                BittrexCurrencyLastResult[] bittrexFormatedData = new BittrexCurrencyLastResult[0];
                if (bittrexCurrenciesData != null) {
                    bittrexFormatedData = bittrexCurrenciesData.getResult();
                }
                if (bittrexFormatedData != null) {
                    for (BittrexCurrencyLastResult bittrexCurrency : bittrexFormatedData) {
                        CommonCurrencyData binanceFormatedData = bittrexCurrency.formatToSave();
                        if(bookmarksPosition.contains(binanceFormatedData.getTag())) {
                            currenciesFormatedData.add(binanceFormatedData);
                        }
                    }
                }
                BookmarkPreferenceHelper.setMarketCurrencies(currenciesFormatedData, ExchangesEnum.BITTREX);
                bookmarksPosition = BookmarkPreferenceHelper.getBookmarkPosition();
                commonCurrenciesData = BookmarkPreferenceHelper.getAllCurrencies();
                if(bookmarkAdapter != null) {
                    bookmarkAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@Nullable Call<BittrexCurrencyLast> call, @Nullable Throwable t) {
                Log.e(ExchangesEnum.BITTREX.getName(), t != null ? t.toString() : "Request failed");
            }
        });
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
