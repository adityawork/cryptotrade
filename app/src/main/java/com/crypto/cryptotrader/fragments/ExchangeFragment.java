package com.crypto.cryptotrader.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.crypto.cryptotrader.R;
import com.crypto.cryptotrader.viewhelpers.ExchangeAdapter;

/**
 * Created by aditya on 14/3/18.
 * Displays the list of available exchanges
 */

public class ExchangeFragment extends Fragment {

    onItemClickCallBack callBack;

    public interface onItemClickCallBack {
        void onItemClick(String tag);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callBack = (onItemClickCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.activity_exchanges, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        GridView exchangeGrid = view.findViewById(R.id.exchanges);
        ExchangeAdapter adapter = new ExchangeAdapter();
        exchangeGrid.setAdapter(adapter);
        exchangeGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                callBack.onItemClick(view.getTag(R.id.EXCHANGE_TAG).toString());
            }
        });
    }
}
