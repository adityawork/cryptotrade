package com.crypto.cryptotrader.models.bittrex;

/**
 * Created by aditya on 20/3/18.
 *
 */

public class BittrexCurrencyLast
{
    @SuppressWarnings("unused")
    private BittrexCurrencyLastResult[] result;
    public BittrexCurrencyLastResult[] getResult () {
        return result;
    }
}