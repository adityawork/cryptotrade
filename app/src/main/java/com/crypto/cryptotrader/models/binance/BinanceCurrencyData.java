package com.crypto.cryptotrader.models.binance;

import com.crypto.cryptotrader.models.ExchangesEnum;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyDataHelper;

import java.util.Locale;

/**
 * Created by aditya on 20/3/18.
 *
 */

public class BinanceCurrencyData extends CommonCurrencyDataHelper
{
    @SuppressWarnings("unused")
    private String symbol;

    @SuppressWarnings("unused")
    private String bidPrice;

    @SuppressWarnings("unused")
    private String priceChangePercent;

    private String getSymbol ()
    {
        return symbol;
    }

    private String getBidPrice ()
    {
        return bidPrice;
    }

    private String getPriceChangePercent ()
    {
        return priceChangePercent;
    }

    public CommonCurrencyData formatToSave()
    {
        double priceChange = Double.valueOf(getPriceChangePercent());
        boolean rise = priceChange > 0;
        String displayPriceChange = String.format(new Locale("en", "INDIA"), "%.2f", priceChange)+"%";
        String symbol = getSymbol();
        String baseCurrency = symbol.substring(symbol.length()-3);
        String currency = symbol.substring(0, (symbol.length()-3));
        String tag = ExchangesEnum.BINANCE.getTagName()+"-"+baseCurrency+"-"+currency;
        String display = currency+"/"+baseCurrency;

        CommonCurrencyData commonCurrencyData = new CommonCurrencyData();
        commonCurrencyData.setCurrencyValue(getBidPrice());
        commonCurrencyData.setTag(tag);
        commonCurrencyData.setRise(rise);
        commonCurrencyData.setDisplayPriceChange(displayPriceChange);
        commonCurrencyData.setExchange(ExchangesEnum.BINANCE.getName());
        commonCurrencyData.setDisplay(display);
        return commonCurrencyData;
    }

    @Override
    public String toString()
    {
        return "[symbol = "+symbol+", bidPrice = "+bidPrice+", priceChangePercent = "+priceChangePercent+"]";
    }
}