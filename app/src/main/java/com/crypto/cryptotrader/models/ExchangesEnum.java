package com.crypto.cryptotrader.models;

import com.crypto.cryptotrader.R;

/**
 * Created by qoini on 04/04/18.
 *
 */

public enum ExchangesEnum {

    BINANCE("BINANCE", "BIN", R.drawable.binance, "bookmark_binance"),
    BITTREX("BITTREX", "BIT", R.drawable.bittrex, "bookmark_bittrex");

    private String name;
    private String tagName;
    private String fileName;
    private int imageResource;

    ExchangesEnum(String name, String tagName, int imageResource, String filename) {
        setName(name);
        setTagName(tagName);
        setImageResource(imageResource);
        setFileName(filename);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String shortName) {
        this.tagName = shortName;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    @Override
    public String toString() {
        return getName();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}