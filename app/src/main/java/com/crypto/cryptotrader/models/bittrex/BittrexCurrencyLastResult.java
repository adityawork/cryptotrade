package com.crypto.cryptotrader.models.bittrex;

import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyDataHelper;
import com.crypto.cryptotrader.models.ExchangesEnum;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;

/**
 * Created by aditya on 21/3/18.
 *
 */

public class BittrexCurrencyLastResult extends CommonCurrencyDataHelper
{
    @SuppressWarnings("unused")
    private String PrevDay;

    @SuppressWarnings("unused")
    private String Bid;

    @SuppressWarnings("unused")
    private String MarketName;

    private String getPrevDay () {
        return PrevDay;
    }

    private String getBid () {
        return Bid;
    }

    private String getMarketName () {
        return MarketName;
    }
    @Override
    public String toString() {
        return "ClassPojo [PrevDay = "+PrevDay+", Bid = "+Bid+", MarketName = "+MarketName+"]";
    }

    public CommonCurrencyData formatToSave() {
        Double bid = Double.parseDouble(getBid());
        Double last = Double.parseDouble(getPrevDay());
        Double relValue = bid - last;
        boolean rise = (relValue >= 0);
        Double relPer = (relValue/bid) * 100;
        String displayPriceChange = String.format(getLocale(), "%.2f", relPer)+"%";
        String currencyTextValue = String.format(getLocale(), "%.7f", bid);
        String[] marketDataArr = getMarketName().split("-");
        String baseCurrency = marketDataArr[0];
        String currency = marketDataArr[1];
        String display = currency+"/"+baseCurrency;
        String tag = ExchangesEnum.BITTREX.getTagName()+"-"+baseCurrency+"-"+currency;

        CommonCurrencyData commonCurrencyData = new CommonCurrencyData();
        commonCurrencyData.setCurrencyValue(currencyTextValue);
        commonCurrencyData.setTag(tag);
        commonCurrencyData.setRise(rise);
        commonCurrencyData.setExchange(ExchangesEnum.BITTREX.getName());
        commonCurrencyData.setDisplayPriceChange(displayPriceChange);
        commonCurrencyData.setDisplay(display);
        return commonCurrencyData;
    }
}