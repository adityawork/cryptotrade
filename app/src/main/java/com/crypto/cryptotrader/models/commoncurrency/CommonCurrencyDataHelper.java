package com.crypto.cryptotrader.models.commoncurrency;

import java.util.Locale;

/**
 * Created by qoini on 04/04/18.
 *
 */

public abstract class CommonCurrencyDataHelper {
    private static final Locale locale = new Locale("en","INDIA");

    public abstract CommonCurrencyData formatToSave();

    protected Locale getLocale() {
        return locale;
    }
}
