package com.crypto.cryptotrader.models.commoncurrency;

/**
 * Created by qoini on 02/04/18.
 *
 */

public class CommonCurrencyData {
    private String exchange;

    private String display;

    private String currencyValue;

    private String tag;

    private boolean rise;

    private String displayPriceChange;


    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getCurrencyValue() {
        return currencyValue;
    }

    public void setCurrencyValue(String currencyValue) {
        this.currencyValue = currencyValue;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString()
    {
        return "[exchange = "+exchange+", display = "+display+", currencyValue = "+currencyValue+", tag = "+tag+"]";
    }

    public boolean isRise() {
        return rise;
    }

    public void setRise(boolean rise) {
        this.rise = rise;
    }

    public String getDisplayPriceChange() {
        return displayPriceChange;
    }

    public void setDisplayPriceChange(String displayPriceChange) {
        this.displayPriceChange = displayPriceChange;
    }
}
