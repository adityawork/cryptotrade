package com.crypto.cryptotrader.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.crypto.cryptotrader.R;

/**
 * Created by qoini on 28/03/18.
 * A media player service to play
 * the podcast on crypto currency
 */

public class PodcastPlayBackService extends Service {
    private PodcastPlayBinder podcastPlayBinder = new PodcastPlayBinder();
    private MediaPlayer mp;
    public static final int PLAY_MODE = 1;
    public static final int PAUSE_MODE = 0;
    private int action;


    @Override
    public void onCreate() {
        mp = MediaPlayer.create(this, R.raw.podcast);
        super.onCreate();
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public class PodcastPlayBinder extends Binder {
        public PodcastPlayBackService getService() {
            return PodcastPlayBackService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return podcastPlayBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        mp.start();
        if(getAction() == PodcastPlayBackService.PAUSE_MODE && mp.isPlaying()) {
            mp.pause();
        } else {
            mp.start();
        }
        return Service.START_STICKY;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        mp.stop();
        super.onDestroy();
    }

    /**
     * Returns the playing status of the
     * Media player
     * @return isPlaying boolean
     */
    public boolean isPlaying() {
        return mp.isPlaying();
    }
}
