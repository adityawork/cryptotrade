package com.crypto.cryptotrader.utils;

import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;

/**
 * Created by qoini on 06/04/18.
 * A class to generate and close
 * a background thread
 */

public class BackgroundThreadHelper {
    private static Handler mBackgroundHandler;

    /**
     * Generates and returns a background thread
     * @return Handler
     */
    public static Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }

    /**
     * Closes a background thread
     * to avoid memory leak
     */
    public static void closeBackgroundHandler(){
        if (mBackgroundHandler != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mBackgroundHandler.getLooper().quitSafely();
            } else {
                mBackgroundHandler.getLooper().quit();
            }
            mBackgroundHandler = null;
        }
    }
}
