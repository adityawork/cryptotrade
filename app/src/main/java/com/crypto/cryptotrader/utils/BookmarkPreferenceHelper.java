package com.crypto.cryptotrader.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.crypto.cryptotrader.CryptTrader;
import com.crypto.cryptotrader.models.commoncurrency.CommonCurrencyData;
import com.crypto.cryptotrader.models.ExchangesEnum;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by qoini on 02/04/18.
 * A common class to handle
 * shared preferences
 */

public class BookmarkPreferenceHelper {
    private static final String bookmarkPositionFile = "bookmarked";
    private static final String fileName = "bookmark_currency";
    public final static int MODE_ADD = 0;
    public final static int MODE_REMOVE  = 1;

    /**
     * Reads and returns bookmark tags/positions
     * of the selected currencies
     * @return List<String>
     */
    public static List<String> getBookmarkPosition() {
        SharedPreferences sharedPref = CryptTrader.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        String currencies = sharedPref.getString(bookmarkPositionFile, "");
        Gson gson = new Gson();
        return gson.fromJson(currencies, new TypeToken<List<String>>(){}.getType());
    }

    /**
     * Updates the bookmark tag/position
     * with a tag as an input along with the
     * add/remove mode
     * @param tag Custom tag of a currency
     * @param mode Read/Write mode
     */
    public static void setBookmarkPosition(String tag, int mode) {
        SharedPreferences sharedPref = CryptTrader.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        String currencies = sharedPref.getString(bookmarkPositionFile, "");
        Gson gson = new Gson();
        List<String> bookmarkPosition = gson.fromJson(currencies, new TypeToken<List<String>>(){}.getType());
        if(bookmarkPosition == null) {
            bookmarkPosition = new ArrayList<>();
        }
        if(mode == MODE_ADD) {
            bookmarkPosition.add(tag);
        } else {
            bookmarkPosition.remove(tag);
        }
        String positions = gson.toJson(bookmarkPosition);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(bookmarkPositionFile, positions).apply();
    }

    /**
     * Stores the market data
     * in an exchange specific variable name
     * under a common shared preference file
     * @param commonCurrencyData List of currencies data
     * @param exchange Exchange of the currencies
     */
    public static void setMarketCurrencies(List<CommonCurrencyData> commonCurrencyData, ExchangesEnum exchange) {
        SharedPreferences sharedPref = CryptTrader.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        Gson gson = new Gson();
        String content = gson.toJson(commonCurrencyData);
        editor.putString(exchange.getFileName(), content);
        editor.apply();
    }

    /**
     * Reads and returns the data of the
     * saved currencies of the requested exchange
     * @param exchangesEnum Provide exchange to get exchange specific data
     * @return List<CommonCurrencyData>
     */
    public static List<CommonCurrencyData> getMarketCurrencies(ExchangesEnum exchangesEnum) {
        SharedPreferences sharedPref = CryptTrader.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        List<CommonCurrencyData> commonCurrencyData = new ArrayList<>();
            String currenciesJson = sharedPref.getString(exchangesEnum.getFileName(),  "");
            if(currenciesJson.length() > 0) {
                Gson gson = new Gson();
                commonCurrencyData = gson.fromJson(currenciesJson, new TypeToken<List<CommonCurrencyData>>(){}.getType());
            }
        return commonCurrencyData;
    }

    /**
     * Returns the saved status of a currency
     * @param tag Custom tag of a currency
     * @return boolean
     */
    public static boolean isSaved(String tag) {
        List bookmarks = getBookmarkPosition();
        return bookmarks !=null && bookmarks.contains(tag);
    }

    /**
     * Returns the data of all the currencies of all the exchanges
     * @return commonCurrenciesData
     */
    public static List<CommonCurrencyData> getAllCurrencies() {
        SharedPreferences sharedPref = CryptTrader.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        List<CommonCurrencyData> commonCurrenciesData = new ArrayList<>();
        List<CommonCurrencyData> commonCurrencyData;
        for(ExchangesEnum exchange : ExchangesEnum.values()) {
            String currenciesJson = sharedPref.getString(exchange.getFileName(), "");
            if(currenciesJson.length() > 0) {
                Gson gson = new Gson();
                commonCurrencyData = gson.fromJson(currenciesJson, new TypeToken<List<CommonCurrencyData>>(){}.getType());

                if(commonCurrencyData.size() > 0) {
                    commonCurrenciesData.addAll(commonCurrencyData);
                }
            }
        }
        return commonCurrenciesData;
    }

    /**
     * Replaces the list of stored bookmarks
     * @param bookmarkPosition List of all the bookmarks
     */
    public static void updateBookmarkPosition(List<String> bookmarkPosition) {
        SharedPreferences sharedPref = CryptTrader.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String positions = gson.toJson(bookmarkPosition);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(bookmarkPositionFile, positions).apply();
    }

    /**
     * Updates a currency data in the stored set of currencies
     * @param selectedCurrency The currency to be updated
     * @param exchange Exchange that currency belongs to
     * @param mode Update mode
     */
    public static void setMarketCurrency(CommonCurrencyData selectedCurrency, ExchangesEnum exchange, int mode) {
        SharedPreferences sharedPref = CryptTrader.getContext().getSharedPreferences(fileName, Context.MODE_PRIVATE);
        List<CommonCurrencyData> currencies = new ArrayList<>();
        String currenciesJson = sharedPref.getString(exchange.getFileName(),  "");
        Gson gson = new Gson();
        if(currenciesJson.length() > 0) {

            currencies = gson.fromJson(currenciesJson, new TypeToken<List<CommonCurrencyData>>(){}.getType());
        }
        if(mode == BookmarkPreferenceHelper.MODE_REMOVE) {
            currencies.remove(selectedCurrency);
        } else {
            currencies.add(selectedCurrency);
        }
        SharedPreferences.Editor editor = sharedPref.edit();
        String content = gson.toJson(currencies);
        editor.putString(exchange.getFileName(), content);
        editor.apply();
    }
}
